extends KinematicBody2D

var motion = Vector2()
var GRAVITY = 20 
var SPEED = 200
var JUMP = -550


func _physics_process(dt):
	
	motion.y += GRAVITY
	
	if Input.is_action_pressed("ui_right"):
		motion.x = SPEED
		$Sprite.play("run")
		$Sprite.flip_h = false
	elif Input.is_action_pressed("ui_left"):
		motion.x = -SPEED
		$Sprite.play("run")
		$Sprite.flip_h = true
	else:
		motion.x = 0
		$Sprite.play("idle")

	if is_on_floor():
		if Input.is_action_just_pressed("ui_up"):
			motion.y = JUMP
	else:
		$Sprite.play("jump")

	motion = move_and_slide(motion, Vector2(0, -1))
	
	# Impede que o player saida da tela.
	#self.position.x = clamp(self.position.x, 0, 300)
















