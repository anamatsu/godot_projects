extends Node2D

var inimigo = preload("res://cenas/inimigo.tscn")
var tempo_instanciar = 0

func _ready():
	randomize() # Almenta a aletoriedade do rand_range()



func _process(delta):
	tempo_instanciar += delta
	if tempo_instanciar > 2:
		spawn() # tempo de renascimento do inimigo
		tempo_instanciar = 0



func spawn():
	# Cria uma instância de inimigo
	var i = inimigo.instance()
	self.add_child(i)
	i.position.x = rand_range(25, 485)