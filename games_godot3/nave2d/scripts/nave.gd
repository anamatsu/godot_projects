extends Area2D

var speed = 300
# Import a cena laser (= a import laser)
var laser = preload("res://cenas/laser.tscn")

func _ready():
	pass


func _input(event):
	if Input.is_action_just_pressed("shooter"):
		disparar()


func _process(delta):
	if Input.is_action_pressed("up"):
		if position.y <= 25:
			position.y = 25
		else:
			position.y -= speed * delta
	
	if Input.is_action_pressed("down"):
		if position.y >= 540:
			position.y = 540
		else:
			position.y += speed * delta
		
	if Input.is_action_pressed("left"):
		if position.x <= 25:
			position.x = 25
		else:
			position.x -= speed * delta
		
	if Input.is_action_pressed("right"):
		if position.x >= 485:
			position.x = 485
		else:
			position.x += speed * delta


func disparar():
	var l = laser.instance()
	var som = $"../laser_audio"
	
	# Cria um novo laser na posição global da nave (laser_position).
	l.position = $laser_position.global_position
	
	# Adiciona o novo laser criado a cena pai da cena nave, ou seja,
	# adiciona o laser na cena principal do jogo.
	get_parent().add_child(l)
	som.play()
	som.volume_db = -30
	
	
	
	