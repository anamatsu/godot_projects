extends Area2D

var vel = 300
var ex = preload("res://cenas/explosao.tscn")

func _ready():
	pass


func _process(delta):
	
	position.y += vel * delta



# Verifica quando o objeto(inimigo) sai da tela e remove da memória
func _on_notifier_screen_exited():
	queue_free()


func _on_inimigo_area_entered(area):
	var som = $"../explosao"
	if area.name != 'nave':
		explosao()
		queue_free()
		som.play()
		som.volume_db = -20
		area.queue_free()


func explosao():
	var explodir = ex.instance()
	explodir.position = self.global_position
	get_parent().add_child(explodir)
	explodir.play()





