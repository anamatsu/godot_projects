extends Area2D

export var vel = 500

func _ready():
	pass


func _process(delta):
	position.y -= vel * delta
	
	# Quando o laser sai da dela ele é removido da memória.
	if position.y < 0:
		queue_free()
